// rollup.config.js

import resolve from "./dist/plugin-node-resolve";
import commonjs from "./dist/plugin-commonjs";
import json from "./dist/plugin-json";
import babel from "./dist/plugin-babel";

export default {
  input: "src/commonjs.js",
  output: {
    file: "test-output.js",
    format: "es",
  },
  plugins: [
    json(),
    resolve(),
    commonjs({
      dynamicRequireTargets: ["node_modules/glob/*.js"],
      transformMixedEsModules: true,
    }),
    babel({ babelHelpers: "bundled" }),
  ],
};
