// rollup.config.js

import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

function makeConfig(name, commonjs_opts = {}, global_opts = {}) {
  return {
    input: `src/${name}.js`,
    output: {
      file: `dist/plugin-${name}.js`,
      format: "esm",
      interop: "auto",
    },
    plugins: [
      json(),
      resolve(),
      commonjs({
        transformMixedEsModules: true,
        ...commonjs_opts,
      }),
    ],
    ...global_opts,
  };
}

export default [
  makeConfig("alias"),
  makeConfig("auto-install"),
  makeConfig("babel"),
  makeConfig("commonjs", { dynamicRequireTargets: ["node_modules/glob/*.js"] }),
  makeConfig("data-uri"),
  makeConfig("html"),
  makeConfig("image"),
  makeConfig("json"),
  makeConfig("multi-entry", {
    dynamicRequireTargets: ["node_modules/glob/*.js"],
  }),
  makeConfig("node-resolve"),
  makeConfig("replace"),
];
